package main

import (
	"bytes"
	"fmt"
	"math/rand"
	"strconv"
	"time"

	"github.com/fatih/color"
)

// Game interface type
type Game interface {
	Play()
}

// GameBuilder interface type
type GameBuilder interface {
	Height(int) GameBuilder
	Width(int) GameBuilder
	Rounds(int) GameBuilder
	Build() Game
}

type gameBuilder struct {
	height int
	width  int
	rounds int
}

/*****************************************************************************
 *Extract game builder********************************************************
 ****************************************************************************/

func (cb *gameBuilder) Height(height int) GameBuilder {
	cb.height = height
	return cb
}

func (cb *gameBuilder) Width(width int) GameBuilder {
	cb.width = width
	return cb
}

func (cb *gameBuilder) Rounds(rounds int) GameBuilder {
	cb.rounds = rounds
	return cb
}

func (cb *gameBuilder) Build() Game {
	return &game{
		height: cb.height,
		width:  cb.width,
		rounds: cb.rounds,
	}
}

// New Generates games
func New() GameBuilder {
	return &gameBuilder{}
}

/*****************************************************************************
 *Extract game****************************************************************
 *****************************************************************************/

type game struct {
	height int
	width  int
	rounds int
}

func (c *game) Play() {
	header := color.New(color.FgGreen).Add(color.Underline)
	alert := color.New(color.FgRed).Add(color.Italic)
	body := color.New(color.FgBlue).Add(color.Italic)

	header.Println("Conway's Game of Life")
	body.Println("Height: " + strconv.Itoa(c.height))
	body.Println("Width " + strconv.Itoa(c.width))
	body.Println("Rounds: " + strconv.Itoa(c.rounds))
	alert.Println("Loading...")
	time.Sleep(time.Second * 2)

	l := NewLife(c.width, c.height)
	for i := 0; i < c.rounds; i++ {
		l.Step()
		fmt.Print("\033[2J\u001b[5A", l)
		time.Sleep(time.Second / 5)
	}

	// err := errors.New("Unable to play :(")
	// if err != nil {
	// 	fmt.Print(err)
	// }
	// return err
}

/*****************************************************************************
 *Extract Field***************************************************************
 *****************************************************************************/

// Field represents a two-dimensional field of cells.
type Field struct {
	s    [][]bool
	w, h int
}

// NewField returns a new, empty field of the specified width and height.
func NewField(w, h int) *Field {
	s := make([][]bool, h)
	for i := range s {
		s[i] = make([]bool, w)
	}
	return &Field{s: s, w: w, h: h}
}

// Set sets the state of the specified cell to the given value.
func (f *Field) Set(x, y int, b bool) {
	f.s[y][x] = b
}

// Alive returns dead or alive status of a cell
func (f *Field) Alive(x, y int) bool {
	defer func() {
		if err := recover(); err != nil {
			return
		}
	}()
	return f.s[y][x]
}

// Next returns the state of the specified cell at the next time step.
func (f *Field) Next(x, y int) bool {
	alive := 0
	for i := -1; i <= 1; i++ {
		for j := -1; j <= 1; j++ {
			if (j != 0 || i != 0) && f.Alive(x+i, y+j) {
				alive++
			}
		}
	}
	return alive == 3 || alive == 2 && f.Alive(x, y)
}

/*****************************************************************************
 *Extract Life****************************************************************
 *****************************************************************************/

// Life stores the state of a round
type Life struct {
	a, b *Field
	w, h int
}

// NewLife gives a new game with random initial state
func NewLife(w, h int) *Life {
	a := NewField(w, h)
	for i := 0; i < (w * h / 4); i++ {
		a.Set(rand.Intn(w), rand.Intn(h), true)
	}
	return &Life{
		a: a, b: NewField(w, h),
		w: w, h: h,
	}
}

// Step advances the game by one instant, updating all cells
func (l *Life) Step() {
	// Update the state of the next field (b) from the current field (a).
	for y := 0; y < l.h; y++ {
		for x := 0; x < l.w; x++ {
			l.b.Set(x, y, l.a.Next(x, y))
		}
	}
	// Swap play fields
	l.a, l.b = l.b, l.a
}

// String returns the game board as a string
func (l *Life) String() string {
	color.Set(color.FgYellow)
	var buf bytes.Buffer

	for y := 0; y < l.h; y++ {
		for x := 0; x < l.w; x++ {
			b := " "
			if l.a.Alive(x, y) {
				b = "X"
			}
			buf.WriteString(b)
		}
		buf.WriteString("\n")
	}
	return buf.String()
}

func main() {
	builder := New()
	game := builder.Height(20).Width(30).Rounds(200).Build()
	game.Play()

	/**
	 * Centralied error handling not advised in Go
	 */

	// res, err := game.Play()

	// if err != nil {
	// 	switch err {
	// 	case bufio.ErrNegativeCount:
	// 		// ...
	// 		return
	// 	case bufio.ErrBufferFull:
	// 		// ...
	// 		return
	// 	default:
	// 		// ...
	// 		return
	// 	}
	// }

	// if err != nil {
	// handle the error
	// }
}
