#!/bin/bash
> results.txt

go test -json >> results.txt

go test -cover >> results.txt

go test -bench=. -run=^a >> results.txt