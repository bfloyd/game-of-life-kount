# Install #
- go get -d ./...

# Run #
- go run app.go

# Run ALL tests #
- bash report.sh

# Run individual tests #

### Test ###
- go test -json >> results.txt

### Coverage ###
- go test -cover >> results.txt

### Benchmark ###
- go test -bench=. -run=^a >> results.txt