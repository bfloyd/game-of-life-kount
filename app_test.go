package main

import (
	"reflect"
	"testing"
)

func Benchmark_gameBuilder_Height(b *testing.B) {
	for n := 0; n < b.N; n++ {
		builder := New()
		builder.Height(5)
	}
}

func Test_gameBuilder_Height(t *testing.T) {
	type args struct {
		height int
	}
	tests := []struct {
		name string
		cb   *gameBuilder
		args args
		want GameBuilder
	}{
		{"One", &gameBuilder{}, args{height: 1}, &gameBuilder{height: 1}},
		{"Two", &gameBuilder{}, args{height: 2}, &gameBuilder{height: 2}},
		{"Three", &gameBuilder{}, args{height: 3}, &gameBuilder{height: 3}},
		{"NintyNine", &gameBuilder{}, args{height: 99}, &gameBuilder{height: 99}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.cb.Height(tt.args.height); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("gameBuilder.Height() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Benchmark_gameBuilder_Width(b *testing.B) {
	for n := 0; n < b.N; n++ {
		builder := New()
		builder.Width(5)
	}
}

func Test_gameBuilder_Width(t *testing.T) {
	type args struct {
		width int
	}
	tests := []struct {
		name string
		cb   *gameBuilder
		args args
		want GameBuilder
	}{
		{"One", &gameBuilder{}, args{width: 1}, &gameBuilder{width: 1}},
		{"Two", &gameBuilder{}, args{width: 2}, &gameBuilder{width: 2}},
		{"Three", &gameBuilder{}, args{width: 3}, &gameBuilder{width: 3}},
		{"NintyNine", &gameBuilder{}, args{width: 99}, &gameBuilder{width: 99}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.cb.Width(tt.args.width); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("gameBuilder.Width() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_gameBuilder_Rounds(t *testing.T) {
	type args struct {
		rounds int
	}
	tests := []struct {
		name string
		cb   *gameBuilder
		args args
		want GameBuilder
	}{
		{"One", &gameBuilder{}, args{rounds: 1}, &gameBuilder{rounds: 1}},
		{"Two", &gameBuilder{}, args{rounds: 2}, &gameBuilder{rounds: 2}},
		{"Three", &gameBuilder{}, args{rounds: 3}, &gameBuilder{rounds: 3}},
		{"NintyNine", &gameBuilder{}, args{rounds: 99}, &gameBuilder{rounds: 99}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.cb.Rounds(tt.args.rounds); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("gameBuilder.Rounds() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Benchmark_gameBuilder_Rounds(b *testing.B) {
	for n := 0; n < b.N; n++ {
		builder := New()
		builder.Rounds(5)
	}
}

func Test_gameBuilder_Build(t *testing.T) {
	tests := []struct {
		name string
		cb   *gameBuilder
		want Game
	}{
		{"One", &gameBuilder{}, &game{}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.cb.Build(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("gameBuilder.Build() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Benchmark_gameBuilder_Build(b *testing.B) {
	for n := 0; n < b.N; n++ {
		builder := New()
		builder.Height(20).Width(30).Rounds(200).Build()
	}
}

func TestNew(t *testing.T) {
	tests := []struct {
		name string
		want GameBuilder
	}{
		{"One", &gameBuilder{}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := New(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("New() = %v, want %v", got, tt.want)
			}
		})
	}
}
